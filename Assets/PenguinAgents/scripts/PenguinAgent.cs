using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class PenguinAgent : Agent
{
    [Tooltip("How fast the agent moves forward")]
    public float moveSpeed = 5f;

    [Tooltip("How fast the agent turns")]
    public float turnSpeed = 180f;

    [Tooltip("Prefab of the heart that appears when the baby is fed")]
    public GameObject heartPrefab;

    [Tooltip("Prefab of the regurgitated fish that appears when the baby is fed")]
    public GameObject regurgitatedFishPrefab;

    private PenguinArea penguinArea;
    new private Rigidbody rigidbody;
    private GameObject baby;
    private bool isFull;

    public override void Initialize() {
        base.Initialize();
        penguinArea = GetComponentInParent<PenguinArea>();
        baby = penguinArea.penguinBaby;
        rigidbody = GetComponent<Rigidbody>();
    }

    public override void OnActionReceived(ActionBuffers actionBuffers) {
        float forwardAmount = actionBuffers.DiscreteActions[0];

        float turnAmount = 0f;
        if (actionBuffers.DiscreteActions[1] == 1f)
            turnAmount = -1f;
        else if (actionBuffers.DiscreteActions[1] == 2f)
            turnAmount = 2f;

        rigidbody.MovePosition(transform.position + transform.forward * forwardAmount * moveSpeed * Time.deltaTime);
        transform.Rotate(transform.up * turnAmount * turnSpeed * Time.fixedDeltaTime);

        if (MaxStep > 0) AddReward(-1f / MaxStep);
    }

    public override void Heuristic(in ActionBuffers actionsOut) {
        int forwardAction = 0;
        int turnAction = 0;
        if (Input.GetKey(KeyCode.W))
            forwardAction = 1;

        if (Input.GetKey(KeyCode.A))
            turnAction = 1;

        if (Input.GetKey(KeyCode.D))
            turnAction = 2;

        actionsOut.DiscreteActions.Array[0] = forwardAction;
        actionsOut.DiscreteActions.Array[1] = turnAction;
    }

    public override void OnEpisodeBegin() {
        isFull = false;
        penguinArea.ResetArea();
    }

    public override void CollectObservations(VectorSensor sensor) {
        sensor.AddObservation(isFull);

        sensor.AddObservation(Vector3.Distance(baby.transform.position, transform.position));

        sensor.AddObservation((baby.transform.position - transform.position).normalized);

        sensor.AddObservation(transform.forward);
    }

    private void OnCollisionEnter(Collision collision) {
        if(collision.transform.CompareTag("fish"))
            EatFish(collision.gameObject);
        else if(collision.transform.CompareTag("baby"))
            RegurgitateFish();
    }

    private void EatFish(GameObject gameObject) {
        if(isFull) return;
        isFull = true;
        
        penguinArea.RemoveSpecificFish(gameObject);

        AddReward(1f);
    }

    private void RegurgitateFish() {
        if (!isFull) return;
        isFull = false;

        GameObject regurgitatedFish = Instantiate<GameObject>(regurgitatedFishPrefab);
        regurgitatedFish.transform.parent = transform.parent;
        regurgitatedFish.transform.position = baby.transform.position;
        Destroy(regurgitatedFish, 4f);

        GameObject heart = Instantiate<GameObject>(heartPrefab);
        heart.transform.parent = transform.parent;
        heart.transform.position = transform.position + Vector3.up;
        Destroy(heart, 4f);

        AddReward(1f);

        if(penguinArea.fishRemaining <= 0)
            EndEpisode();
    }
}
