using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
public float agentRunSpeed;

public float agentRotationSpeed;
public Material goalScoredMaterial;
public Material failMaterial;
}
