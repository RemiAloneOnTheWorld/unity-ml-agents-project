using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class AgentController : Agent
{
    public GameObject ground;
    public GameObject area;
    public GameObject symbolO;
    public GameObject symbolX;
    public GameObject symbolStar;
    public GameObject symbolT;
    public bool useVectorObs;
    Rigidbody m_AgentRb;
    Material m_GroundMaterial;
    Renderer m_GroundRenderer;
    Settings m_settings;
    int m_selection;

    public override void Initialize() {
        m_settings = FindObjectOfType<Settings>();
        m_AgentRb = GetComponent<Rigidbody>();
        m_GroundRenderer = ground.GetComponent<Renderer>();
        m_GroundMaterial = m_GroundRenderer.material;
    }

    public override void OnEpisodeBegin() {
        var agentOffSet = -5f;
        m_selection = Random.Range(0,4);

        setUpSymbols();

        transform.position = new Vector3(0f + Random.Range(-3f, 3f), 1f, agentOffSet + Random.Range(-5f, 5f)) + ground.transform.position;
        transform.rotation = Quaternion.Euler(0f, Random.Range(0f,360f), 0f);
        m_AgentRb.velocity *= 0f;
    }

    private void setUpSymbols() {
        symbolO.SetActive(false);
        symbolX.SetActive(false);
        symbolT.SetActive(false);
        symbolStar.SetActive(false);
        switch (m_selection)
        {
            case 0:
                symbolO.SetActive(true);
            break;

            case 1:
                symbolX.SetActive(true);
            break;
            
            case 2:
                symbolStar.SetActive(true);
            break;
            
            case 3:
                symbolT.SetActive(true);
            break;
            
            default:
                m_selection = 0;
                symbolO.SetActive(true);
            break;
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        if (useVectorObs)
        {
            sensor.AddObservation(StepCount / (float)MaxStep);
        }
    }

    IEnumerator GoalScoredSwapGroundMaterial(Material material, float time) {
        m_GroundRenderer.material = material;
        yield return new WaitForSeconds(time);
        m_GroundRenderer.material = m_GroundMaterial;
    }

    private void OnCollisionEnter(Collision col) {
        if(col.gameObject.CompareTag("Symbol_O_Goal") || col.gameObject.CompareTag("Symbol_X_Goal") || col.gameObject.CompareTag("Symbol_*_Goal") || col.gameObject.CompareTag("Symbol_T_Goal")) {
            if ((m_selection == 0 && col.gameObject.CompareTag("Symbol_O_Goal")) || (m_selection == 1 && col.gameObject.CompareTag("Symbol_X_Goal")) || (m_selection == 2 && col.gameObject.CompareTag("Symbol_*_Goal")) || (m_selection == 3 && col.gameObject.CompareTag("Symbol_T_Goal"))) {
                SetReward(1f);
                StartCoroutine(GoalScoredSwapGroundMaterial(m_settings.goalScoredMaterial, 0.5f));
            } else {
                SetReward(-0.1f);
                StartCoroutine(GoalScoredSwapGroundMaterial(m_settings.failMaterial, 0.5f));
            }
            EndEpisode();
        }
    }

    public override void OnActionReceived(ActionBuffers actionBuffers) {
        AddReward(-1f/MaxStep);
        MoveAgent(actionBuffers.DiscreteActions);
        StrafeAgent(actionBuffers.DiscreteActions);
        RotateAgent(actionBuffers.DiscreteActions);
    }

    public override void Heuristic(in ActionBuffers actionsOut) {
        var discreteActionsOut = actionsOut.DiscreteActions;
        if (Input.GetKey(KeyCode.D)) {
            discreteActionsOut[1] = 2;
        }
        if (Input.GetKey(KeyCode.W)) {
            discreteActionsOut[0] = 1;
        }
        if (Input.GetKey(KeyCode.A)) {
            discreteActionsOut[1] = 1;
        }
        if (Input.GetKey(KeyCode.S)) {
            discreteActionsOut[0] = 2;
        }
        if (Input.GetKey(KeyCode.E)) {
            discreteActionsOut[2] = 1;
        }
        if (Input.GetKey(KeyCode.Q)) {
            discreteActionsOut[2] = 2;
        }
    }

    /// <summary>
    /// Moves the agent forwards and backwards according to the selected action.
    /// </summary>
    public void MoveAgent(ActionSegment<int> act) {
        var dirToGo = Vector3.zero;
        var action = act[0];

        switch (action)
        {
            case 1:
                dirToGo = transform.forward * 1f;
                break;
            case 2:
                dirToGo = transform.forward * -1f;
                break;
        }
        m_AgentRb.AddForce(dirToGo * m_settings.agentRunSpeed, ForceMode.VelocityChange);
    }

    /// <summary>
    /// Moves the agent left or right according to the selected action.
    /// </summary>
    public void StrafeAgent(ActionSegment<int> act) {
        var dirToGo = Vector3.zero;
        var action = act[1];

        switch (action)
        {
            case 1:
                dirToGo = transform.right * -1f;
                break;
            case 2:
                dirToGo = transform.right * 1f;
                break;
        }
        m_AgentRb.AddForce(dirToGo * m_settings.agentRunSpeed, ForceMode.VelocityChange);
    }

    /// <summary>
    /// Rotates the agent according to the selected action.
    /// </summary>
    public void RotateAgent(ActionSegment<int> act) {
        var rotateDir = Vector3.zero;
        var action = act[2];

        switch (action)
        {
            case 1:
                rotateDir = transform.up * 1f;
                break;
            case 2:
                rotateDir = transform.up * -1f;
                break;
        }
        transform.Rotate(rotateDir, Time.deltaTime * 150f);
    }
}